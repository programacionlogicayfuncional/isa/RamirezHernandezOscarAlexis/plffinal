(ns plffinal.core
  (:gen-class)
  (:require [plffinal.diccionario :as d]
            [clojure.string :as str]))
;-------------------------------------------------------------------------------

(defn coll-a-vector
  [& args]
  (into [] (map char (apply str (concat args)))))

(defn direccion
  [s]
  (let [y (fn [x] (map d/opciones (coll-a-vector x)))]
    (y s)))

(defn filtra-por
  [pred & args]
  (count (filter pred (map char (coll-a-vector (into [] args))))))

(defn frecuencia
  ([] [])
  ([c & args]
   (let [arriba (fn [p] (= p \^))
         abajo (fn [p] (= p \v))
         izquierda (fn [p] (= p \<))
         derecha (fn [p] (= p \>))
         norte (filtra-por arriba (into [] args))
         sur (filtra-por abajo (into [] args))
         este (filtra-por derecha (into [] args))
         oeste (filtra-por izquierda (into [] args))]
     ({\^ norte \v sur \< oeste \> este} c))))


;;regresa-al-punto-de-origen? 1
(defn regresa-al-punto-de-origen?
  [s]
  (cond
    (and (= (frecuencia \< s) (frecuencia \> s))
         (= (frecuencia \^ s) (frecuencia \v s)))
    true
    :else false))
;2
(defn regresan-al-punto-de-origen?
  [& args]
  (cond
    (and (= (frecuencia \< args) (frecuencia \> args))
         (= (frecuencia \^ args) (frecuencia \v args)))
    true
    :else false))
;4
(defn mismo-punto-final?
  [xs ys]
  (cond
    (and (= (frecuencia \< xs ys) (frecuencia \> xs ys))
         (= (frecuencia \^ xs ys) (frecuencia \v xs ys)))
    true
    :else false))
(mismo-punto-final? (list \< \v \>) (list \> \v \<))

